all: libopencm3 clean
	make -C src all CFILES="sdtasks.c ../examples/blink/blink-task.c"

libopencm3:
	make -C libopencm3

clean:
	make -C src clean

flash:
	make -C src flash

st-flash:
	make -C src st-flash

.PHONY: libopencm3 all clean flash st-flash
