/**
 * @file blink-task.c
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief blink with frequency change
 * @version 0.1.0
 * @date 2021-02-09
 * 
 * platformio.ini
 *  lib_deps = 
 *      Serial-libOpenCM3
 *      SDTasks-libOpenCM3
 *  monitor_speed = 115200
 *  build_flags = 
 *      -DUSART_USING="USE_USART1" ; use USART1 only
 *      -DMAX_TASKS="3"            ; max tasks at the time (default 10)
 * 
 * @copyright Copyright (c) 2021
 * 
 * libOpenCM3 SysTick Driven Tasks is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * libOpenCM3 SysTick Driven Tasks is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libOpenCM3 SysTick Driven Tasks. If not, see 
 * <http://www.gnu.org/licenses/>.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <serial.h>
#include <sdtasks.h>

uint8_t _id_blink; // save task id

void init_task(uint8_t id);
void blink(uint8_t id);
void change_freq(uint8_t id);
void show_seconds(uint8_t id);

int main(void)
{
    // seting the sysclk
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
    
    // PC13 (Blue Pill LED)
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

    // Serial init
    serial_begin(USART1, BAUD115K2);

    // task_init() setup the systick for 1ms interrupt
    tasks_init();
    // adding a one-time task:
    task_add(init_task, 50);
    // show time stamp in the serial monitor every 1s
    task_add(show_seconds, 1000);

    while (true)
    {
        tasks_run(); // run the tasks
    }
}

/*
 * One-time Task, adding blink and change_freq tasks
 */
void init_task(uint8_t id)
{
    // A one time task shoud remove itself from the queue:
    task_remove(id); 
    serial_puts(USART1, "\rSysTick Driven Tasks\r\n");
    // keep blink task id
    _id_blink = task_add(blink, 250);
    // change blink frequency every 5s
    task_add(change_freq, 5000);
}

/*
 * Toggle de BluePill LED
 */
void blink(__unused uint8_t id)
{
    gpio_toggle(GPIOC, GPIO13);
}

/*
 * Change blink frequency every 5s: 250->500->150->repeat
 */
void change_freq(__unused uint8_t id)
{
    static uint32_t time = 250;
    switch (time)
    {
        case 250: task_change_period(_id_blink, time = 500); break;
        case 500: task_change_period(_id_blink, time = 150); break;
        case 150: task_change_period(_id_blink, time = 250); break;
    }
}

/*
 *  Showing time at Serial Monitor
 */
void show_seconds(__unused uint8_t id)
{
    uint32_t seconds = get_millis() / 1000;
    serial_puts(USART1, "Time: ");
    serial_write(USART1, seconds / 1000 % 10 + '0');
    serial_write(USART1, seconds / 100 % 10 + '0');
    serial_write(USART1, seconds / 10 % 10 + '0');
    serial_write(USART1, seconds % 10 + '0');
    serial_puts(USART1, "s\r");
}
