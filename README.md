# SysTick Driven Tasks for libOpenCM3

This is not an RTOS, It's a simple timer-driven tasks managment. The SysTick has a 1ms configuration and an array of task is executed.

## Brief description

You have to add `<sdtasks.h>` to your project, then in your `main()` function call `tasks_init()` to configure the **SysTick** for 1ms interrupt. After that you can simplly _add_ tasks with `task_add(task, period)` and you must call `tasks_run()` in the _main loop_:

```C
//(...)
#include <sdtasks.h>
//(...)
void myTask(uint8_t id)
{
    //(...)
}
//(...)
int main(void)
{
    //(...)
    tasks_init();
    tasks_add(my_task, 500); // every 500ms
    //(...)
    while(true)
    {
        tasks_run();
    }
}
```

Every task must be `void name_of_task(uint8_t id)`. The `id` it's given by the `task_add()` function. If you don't need to use the `id` you may add `__unused` attribute to avoid warnings at compile time.

## Functions

* `tasks_init()`: Initializes the systick with 1ms interrupt.
* `tasks_run()`: must be call at _main loop_.
* `task_add()`: add a task to the queue.
* `task_remove()`: remove a task from the queue.
* `task_change_period()`: change the frequency of the task.
* `task_delay()`: add a _period delay_ to the task an the time it's call.

## Adding to your project

This is ment to be used with [PlatformIO](http://platformio.org/) adding in your `platformio.ini` this:

```bash
lib_deps = SDTasks-libOpenCM3
```

### Optional

In PlatformIO you can edit the `platformio.ini` to add the max-tasks variable to the queue:

```bash
build_flags = -DMAX_TASKS="4"
```

If not, the _default_ value is `"10"`.
