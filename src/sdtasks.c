/**
 * @file sdtasks.c
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Tareas periódicas dirigidas por el SysTick.
 * @version 0.1.0
 * @date 2021-02-08
 * 
 * @copyright Copyright (c) 2021
 *
 * libOpenCM3 SysTick Driven Tasks is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * libOpenCM3 SysTick Driven Tasks is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libOpenCM3 SysTick Driven Tasks. If not, see 
 * <http://www.gnu.org/licenses/>.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/nvic.h>
#include "sdtasks.h"

volatile uint32_t _millis;
volatile task_t _task_array[MAX_TASKS];

void sys_tick_handler(void)
{
    _millis++;
}

uint32_t get_millis(void)
{
    return _millis;
}

void tasks_init(void)
{
    systick_set_frequency(1000, rcc_ahb_frequency);
    systick_counter_enable();
    systick_interrupt_enable();
}

void tasks_run(void)
{
    static uint32_t last_millis = 0;
    uint32_t actual_millis = _millis;
    if (actual_millis != last_millis)
    {
        for (uint8_t i = 0; i < MAX_TASKS; ++i)
        {
            if (_task_array[i].period != 0 && _task_array[i].function != NULL)
            {
                if ((actual_millis - _task_array[i].last_exe) >= _task_array[i].period)
                {
                    _task_array[i].last_exe = actual_millis;
                    _task_array[i].function(i);
                }
            }
        }
        last_millis = actual_millis;
    }
}

uint16_t task_add(void (*function)(uint8_t), uint32_t period)
{
    uint16_t id = -1;
    if (function != NULL && period != 0)
    {
        for (id = 0; id < MAX_TASKS; id++)
        {
            if (_task_array[id].function == NULL)
            {
                _task_array[id].function = function;
                _task_array[id].period = period;
                _task_array[id].last_exe = _millis;
                break;
            }
        }
        if (id == MAX_TASKS)
            id = -1;
    }
    return id;
}

bool task_remove(uint8_t id)
{
    bool result = false;
    if (id < MAX_TASKS && _task_array[id].function != NULL)
    {
        _task_array[id].function = NULL;
        result = true;
    }
    return result;
}

bool task_change_period(uint8_t id, uint32_t new_period)
{
    bool result = false;
    if (id < MAX_TASKS && _task_array[id].function != NULL)
    {
        _task_array[id].period = new_period;
        result = true;
    }
    return result;
}

bool task_delay(uint8_t id)
{
    bool result = false;
    if (id < MAX_TASKS && _task_array[id].function != NULL)
    {
        _task_array[id].last_exe = _millis;
        result = true;
    }
    return result;
}
