/**
 * @file sdtasks.h
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Tareas periódicas dirigidas por el SysTick.
 * @version 0.1.0
 * @date 2021-02-08
 * 
 * @copyright Copyright (c) 2021
 *
 * libOpenCM3 SysTick Driven Tasks is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * libOpenCM3 SysTick Driven Tasks is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser 
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with libOpenCM3 SysTick Driven Tasks. If not, see 
 * <http://www.gnu.org/licenses/>.
 * 
 */
#ifndef __SDTASKS_H__
#define __SDTASKS_H__

#include <libopencm3/cm3/systick.h>
#include <stdlib.h>

#ifndef MAX_TASKS
  #define MAX_TASKS 10
#endif//MAX_TASKS

typedef struct {
    void (*function)(uint8_t);
    uint32_t period;
    uint32_t last_exe;
} task_t;

BEGIN_DECLS

/**
 * @brief Get the millis
 * 
 * @return uint32_t millis from systick
 */
uint32_t get_millis(void);

/**
 * @brief Init the systick for 1ms interrupt
 */
void tasks_init(void);

/**
 * @brief You must call this in the main loop
 */
void tasks_run(void);

/**
 * @brief Adding a task to the queue
 * 
 * @param function task to execute
 * @param period how often
 * @return uint16_t id of the task -1 if there was an error and not added.
 */
uint16_t task_add(void (*function)(uint8_t), uint32_t period);

/**
 * @brief Remove a task from the queue
 * 
 * @param id of the task to be remove
 * @return true if the task was successfully remove
 * @return false if the task didn't exist
 */
bool task_remove(uint8_t id);

/**
 * @brief change frequency of a task
 * 
 * @param id task to be change
 * @param new_period new period value
 * @return true the task has successfully changed its period
 * @return false the task didn't exist
 */
bool task_change_period(uint8_t id, uint32_t new_period);

/**
 * @brief delay a task
 * 
 * Add a period time delay to the task.
 * 
 * @param id the task to be delayed
 * @return true if the task was delayed
 * @return false the task didn't exist
 */
bool task_delay(uint8_t id);

END_DECLS

#endif//__SDTASKS_H__
